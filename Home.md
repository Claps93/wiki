# Welcome
Welcome to the Spring for Delphi wiki! Spring4D is a Delphi-based framework that contains a number of different modules including:

* A set of interface-based [Collections](Collections.md)
* A powerful [IEnumerable](IEnumerable.md) interface for accessing those collections
* A [Dependency Injection Container](Dependency Injection.md)
* An Encryption Library
* A Reflection library that extends Delphi's RTTI

# Credits
Spring4D was originally written by Baoquan Zuo and is now maintained by Stefan Glienke.  This wiki is maintained by [Nick Hodges](http://www.nickhodges.com).  Pull requests, particularly for the wiki, are very welcome.  :-)

# This Wiki
This wiki is a git project hosted on GitHub.  If you'd like to help, please feel free to submit a pull request.    The project can be found at:

    git clone git@bitbucket.org:sglienke/spring4d.git/wiki

We'd love any help we can get for this wiki documentation.  You can edit this project like a Git repository, or you can edit it right online as a traditional wiki.  Either way, we are happy for your contributions.

# Code Documentation
Code documentation for Spring4D is [online at DevJet Software](http://www.devjetsoftware.com/docs/spring4d/).  This documentation is derived from the xmldoc comments in the code.  It's also a demonstration of the excellent [Documentation Insight](http://www.devjetsoftware.com/products/documentation-insight/) tool from [DevJet Software](http://www.devjetsoftware.com/).  

That documentation is documentation for the code itself.  This wiki is designed to be more general documentation consisting of explanations, how-to's, tutorials, etc.  

# Articles

[Using Spring4D with C++](CppSpring4d.md) -- This article describes how to use RAD Studio to write applications in Delphi and C++ combined and use Spring container to inject implementations written in either language.

