# Unit test summary

As of `20140622`

Summary:

> All unit tests for Delphi XE..XE6 succeed on the `Win32` and `Win64` platforms.  

Note:

> Tests on the OSX, iOS and Android platforms are not yet ran automatically, so not included in this summary yet.

    Tests\Bin\DelphiXE\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE2\Win32\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE2\Win32\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE2\Win64\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE2\Win64\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE3\Win32\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE3\Win32\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE3\Win64\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE3\Win64\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE4\Win32\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE4\Win32\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE4\Win64\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE4\Win64\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE5\Win32\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE5\Win32\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE5\Win64\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE5\Win64\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE6\Win32\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE6\Win32\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE6\Win64\Debug\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
    Tests\Bin\DelphiXE6\Win64\Release\Spring.Tests.Reports.xml
    <stat name="failures" value="0" />
 